﻿using System;
using DiagnosticManagement.BusinessLogics;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.UserInterface
{
    public partial class Payment : System.Web.UI.Page
    {
        BillManager _billManager=new BillManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            Bill bill = _billManager.GetBillByNo(billNoTextBox.Text);
            GridView1.DataSource = bill.Patient.Tests;
            GridView1.DataBind();
            dateLabel.Text = bill.BillDate.ToString("yyyy-MM-dd");
            totalAmountLabel.Text = bill.TotalAmmount.ToString("#,###.##");
            pAmountLabel.Text = bill.PaidAmmount.ToString("#,###.##");
            dAmountLabel.Text = bill.DueAmmount.ToString("#,###.##");

            ViewState["Bill"] = bill;
        }

        protected void payButton_Click(object sender, EventArgs e)
        {
            Bill bill = (Bill) ViewState["Bill"];
            Response.Write(_billManager.PayBill(bill, Convert.ToDouble(amountTextBox.Text)));
            pAmountLabel.Text = bill.PaidAmmount.ToString("#,###.##");
            dAmountLabel.Text = bill.DueAmmount.ToString("#,###.##");
        }
    }
}