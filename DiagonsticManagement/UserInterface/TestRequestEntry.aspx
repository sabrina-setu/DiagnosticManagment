﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestRequestEntry.aspx.cs" Inherits="DiagnosticManagement.UserInterface.TestRequestEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Care BITM|Test Request Entry</title>
    <link href="Style/Theme.css" rel="stylesheet" />
</head>
<body style="background-image:url('../resourses/images2.jpg')">
    <form id="form1" runat="server">
      <p class="p">BITM Care Bill Management System</p>
    <div class="box">

        <table>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="home_Button" PostBackUrl="../UserInterface/Home.aspx" CssClass="button" runat="server" Text="Home" Width="80px" style="text-align:left;"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testTypesetup_Button" PostBackUrl="../UserInterface/TestTypeSetup.aspx" CssClass="button"  runat="server" href="TestTypeSetup.aspx" Text="Test Type Setup" Width="150px" />
               </td> 
               <td class="auto-style2">
                    <asp:Button ID="testSetup_Button" PostBackUrl="../UserInterface/TestSetup.aspx" CssClass="button"  runat="server" Text="Test Setup" Width="120px"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testRequestEntry_Button" Enabled="False" CssClass="button" runat="server" Text="Test Request Entry" Width="170px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="payment_Button" PostBackUrl="../UserInterface/Payment.aspx" CssClass="button" runat="server" href="Payment.aspx" Text="Payment" Width="120px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="testWiseReport_Button" PostBackUrl="../UserInterface/TestWiseReport.aspx" CssClass="button" runat="server" Text="Test Wise Report" Width="150px"/>
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="typeWiseReport_Button" PostBackUrl="../UserInterface/TypeWiseReport.aspx" CssClass="button" runat="server" Text="Type Wise Report" Width="150px" />
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="unpaidBillReport_Button" PostBackUrl="../UserInterface/UnpaidBillReport.aspx" CssClass="button" runat="server" Text="Unpaid Bill Report" Width="150px" />
                 </td>
           </tr>
        </table>
         <div class="container" style="height:700px" >
             <table>
                 <tr>
                 <asp:Label ID="testRequestEntryLabel" runat="server" CssClass="label" style="margin-left:200px; font-weight:bold; font-size:24px;" Text="Test Request Entry"></asp:Label>
                 </tr>
              </table>
                 <table class="auto-style1">
                     <tr>
                         <td>
                             <asp:Label ID="nameOfThePatientLabel" runat="server" CssClass="label" Text="Name Of The Patient"></asp:Label>
                         </td>
                         <td class="auto-style2">
                             <asp:TextBox ID="nameOfThePatientTextBox" runat="server" CssClass="input" width="300px"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td>
                             <asp:Label ID="dateOfBirthLabel" runat="server" CssClass="label" Text="Date Of Birth"></asp:Label>
                         </td>
                         <td class="auto-style2">
                             <asp:TextBox ID="dateOfBirthTextBox" runat="server" CssClass="input" Width="300px"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td>
                             <asp:Label ID="mobileNoLabel" runat="server" CssClass="label" Text="Mobile No"></asp:Label>
                         </td>
                         <td class="auto-style2">
                             <asp:TextBox ID="mobileNoTextBox" runat="server" CssClass="input" Width="300px"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td>
                             <asp:Label ID="selectTestLabel" runat="server" CssClass="label" Text="Select Test"></asp:Label>
                         </td>
                         <td class="auto-style2">
                      <asp:DropDownList ID="selectTestDropDownList" runat="server" CssClass="input" Width="300px" OnSelectedIndexChanged="selectTestDropDownList_SelectedIndexChanged" OnTextChanged="selectTestDropDownList_SelectedIndexChanged"></asp:DropDownList>
                         </td>
                     </tr>
                     <tr>
                         <td>&nbsp;</td>
                         <td class="auto-style2">
                             <asp:Label ID="feeLabel" runat="server" CssClass="label" Style="margin-left:160px; margin-right:15px;" Text="Fee"></asp:Label>
                             <asp:TextBox ID="feeTextBox" runat="server" CssClass="input" Width="95px" Enabled="False"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td>&nbsp;</td>
                         <td class="auto-style2">
                             <asp:Button ID="addButton" runat="server" CssClass="button" Style="margin-left:230px;margin-top:10px;" Text="ADD" OnClick="addButton_Click" />
                         </td>
                     </tr>
                 </table>
                        <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" Width="80%" style="margin-left: 10%" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                            <Columns>
                                <asp:TemplateField HeaderText="SL">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                            <SortedAscendingCellStyle BackColor="#F4F4FD" />
                            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                            <SortedDescendingCellStyle BackColor="#D8D8F0" />
                            <SortedDescendingHeaderStyle BackColor="#3E3277" />
                        </asp:GridView>
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="totalLabel" runat="server" CssClass="label" style="margin-left:560px;margin-top:10px;" Text="Total" Width="77px"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="totalTextBox" runat="server" CssClass="input" width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                       <asp:Button ID="saveButton" runat="server" CssClass="button" Style="margin-left:60px;margin-top:10px;" Text="Save" OnClick="saveButton_Click" />
                </td>
            </tr>
        </table>

    </div>
    </div>
    </form>
</body>
</html>
