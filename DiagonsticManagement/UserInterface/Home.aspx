﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="DiagnosticManagement.UserInterface.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Care BITM|Home</title>
    <link href="Style/Theme.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 10px;
            height: 23px;
        }
        .auto-style2 {
            height: 23px;
        }
    </style>
</head>
<body style="background-image:url('../resourses/images4.jpg')">
    <form id="form1" runat="server">
    <p class="p">BITM Care Bill Management System</p>
    <div class="box" style="height:400px;">
        <table>
            <tr>
                <td class="auto-style2">
                    <asp:Button Enabled="False" ID="home_Button" CssClass="button" runat="server" Text="Home" Width="80px" style="text-align:left;"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testTypesetup_Button" PostBackUrl="../UserInterface/TestTypeSetup.aspx"  CssClass="button"  runat="server" href="TestTypeSetup.aspx" Text="Test Type Setup" Width="150px" /> 
               </td> 
               <td class="auto-style2">
                    <asp:Button ID="testSetup_Button" PostBackUrl="../UserInterface/TestSetup.aspx" CssClass="button"  runat="server" Text="Test Setup" Width="120px"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testRequestEntry_Button" PostBackUrl="../UserInterface/TestRequestEntry.aspx" CssClass="button" runat="server" Text="Test Request Entry" Width="170px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="payment_Button" PostBackUrl="../UserInterface/Payment.aspx" CssClass="button" runat="server" href="Payment.aspx" Text="Payment" Width="120px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="testWiseReport_Button" PostBackUrl="../UserInterface/TestWiseReport.aspx" CssClass="button" runat="server" Text="Test Wise Report" Width="150px"/>
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="typeWiseReport_Button" PostBackUrl="../UserInterface/TypeWiseReport.aspx" CssClass="button" runat="server" Text="Type Wise Report" Width="150px" />
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="unpaidBillReport_Button" PostBackUrl="../UserInterface/UnpaidBillReport.aspx" CssClass="button" runat="server" Text="Unpaid Bill Report" Width="150px" />
                 </td>
           </tr>
            </table>
       <div>
        <img src="../resourses/d2.png" class="img" />
       </div>
    </div>
    </form>
</body>
</html>
