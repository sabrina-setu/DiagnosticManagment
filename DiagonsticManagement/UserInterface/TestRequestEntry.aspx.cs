﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using DiagnosticManagement.BusinessLogics;
using DiagnosticManagement.DataAccess.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DiagnosticManagement.UserInterface
{
    public partial class TestRequestEntry : System.Web.UI.Page
    {
        TestManager _testManager=new TestManager();
        PatientManager _patientManager=new PatientManager();
        BillManager _billManager=new BillManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<Test> allTests = _testManager.GetAllTests();
                selectTestDropDownList.DataValueField = "ID";
                selectTestDropDownList.DataTextField = "Name";
                selectTestDropDownList.DataSource = allTests;
                selectTestDropDownList.DataBind();

                if (ViewState["Patient"] == null) ViewState["Patient"] = new Patient();
                totalTextBox.Text = "0";
            }
            if (ViewState["Patient"] == null) ViewState["Patient"] = new Patient();
            totalTextBox.Text = "0";
            Patient patient = (Patient) ViewState["Patient"];
            GridView1.DataSource = patient.Tests;
            GridView1.DataBind();
        }

        protected void addButton_Click(object sender, EventArgs e)
        {
            Patient patient = (Patient)ViewState["Patient"];
            patient.Tests.Add(_testManager.GetTestById(Convert.ToInt32(selectTestDropDownList.SelectedValue)));
            ViewState["Patient"] = patient;
            GridView1.DataSource = patient.Tests;
            GridView1.DataBind();
            totalTextBox.Text = patient.TotalFee().ToString("#,###.##");
        }

        protected void selectTestDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            feeTextBox.Text = _testManager.GetTestById(Convert.ToInt32(selectTestDropDownList.SelectedValue)).Fee.ToString("#,###.##");
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            Patient patient = (Patient)ViewState["Patient"];
            patient.Name = nameOfThePatientTextBox.Text;
            patient.Dob = Convert.ToDateTime(dateOfBirthTextBox.Text);
            patient.Contact = mobileNoTextBox.Text;

            _patientManager.AddPatient(patient);

            patient.Id = _patientManager.GetLastPatientId();
            Bill bill=new Bill(patient);
            
            _billManager.SetBill(bill);

            GenPdf("BITM Care Bill Management\nBill No.: " + bill.BillNo+"\n\n",
                patient.Tests, "Total: "+patient.TotalFee().ToString("#,###.##")+"\n\nDate: " + DateTime.Now);
        }

        protected void GenPdf(string head, List<Test> tests , string foot)
        {
            Document export = new Document(PageSize.A4, 10, 10, 30, 30);
            MemoryStream memoryStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(export, memoryStream);
            export.Open();
            Paragraph heading = new Paragraph(head);
            heading.Alignment = Element.ALIGN_CENTER;
            export.Add(heading);

            PdfPTable table = new PdfPTable(tests.Count);

            table.AddCell(new Phrase("SL No."));
            table.AddCell(new Phrase("Test Name"));
            table.AddCell(new Phrase("Fee"));
            table.HeaderRows = 1;

            for(int i=0;i<tests.Count;i++)
            {
                table.AddCell(new Phrase((i+1).ToString()));
                table.AddCell(new Phrase(tests[i].Name));
                table.AddCell(new Phrase(tests[i].Fee.ToString("#,###.##")));
            }

            export.Add(table);
            Paragraph total = new Paragraph(foot);
            total.Alignment = Element.ALIGN_RIGHT;
            export.Add(total);
            export.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=Report.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }

    }
}