﻿using System;
using DiagnosticManagement.BusinessLogics;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.UserInterface
{
    public partial class TestTypeSetup : System.Web.UI.Page
    {
        TestTypeManager _testTypeManager=new TestTypeManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = _testTypeManager.GetAllTestTypes();
            GridView1.DataBind();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            TestType testType=new TestType();
            testType.TypeName = typeNameTextBox.Text;
            Response.Write(_testTypeManager.AddTestType(testType));

            GridView1.DataSource = _testTypeManager.GetAllTestTypes();
            GridView1.DataBind();

            typeNameTextBox.Text = null;
        }
    }
}