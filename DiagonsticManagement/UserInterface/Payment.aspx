﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="DiagnosticManagement.UserInterface.Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Care BITM|Payment</title>
    <link href="Style/Theme.css" rel="stylesheet" />
</head>
<body style="background-image:url('../resourses/images3.jpg')">
    <form id="form1" runat="server">
         <p class="p">BITM Care Bill Management System</p>
    <div class="box">
        <table>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="home_Button" PostBackUrl="../UserInterface/Home.aspx" CssClass="button" runat="server" Text="Home" Width="80px" style="text-align:left;"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testTypesetup_Button" PostBackUrl="../UserInterface/TestTypeSetup.aspx" CssClass="button"  runat="server" href="TestTypeSetup.aspx" Text="Test Type Setup" Width="150px" />
               </td> 
               <td class="auto-style2">
                    <asp:Button ID="testSetup_Button" PostBackUrl="../UserInterface/TestSetup.aspx" CssClass="button"  runat="server" Text="Test Setup" Width="120px"/>
                </td>
                <td class="auto-style2">
                    <asp:Button ID="testRequestEntry_Button" PostBackUrl="../UserInterface/TestRequestEntry.aspx" CssClass="button" runat="server" Text="Test Request Entry" Width="170px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="payment_Button" Enabled="False" CssClass="button" runat="server" href="Payment.aspx" Text="Payment" Width="120px"/>
                </td>
                <td class="auto-style1">
                    <asp:Button ID="testWiseReport_Button" PostBackUrl="../UserInterface/TestWiseReport.aspx" CssClass="button" runat="server" Text="Test Wise Report" Width="150px"/>
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="typeWiseReport_Button" PostBackUrl="../UserInterface/TypeWiseReport.aspx" CssClass="button" runat="server" Text="Type Wise Report" Width="150px" />
                </td>
                 <td class="auto-style2">
                    <asp:Button ID="unpaidBillReport_Button" PostBackUrl="../UserInterface/UnpaidBillReport.aspx" CssClass="button" runat="server" Text="Unpaid Bill Report" Width="150px" />
                 </td>
           </tr>
        </table>
        
        <div class="container" style="height:700px;">
            <table>
           <tr>
           <asp:Label ID="payBillLabel" runat="server" CssClass="label" style="margin-left:200px; font-weight:bold; font-size:24px;" Text="Pay Bill"></asp:Label>
           </tr> 
           </table>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="billNoLabel" runat="server" CssClass="label" text="Bill No" Width="90px"></asp:Label>
                    </td>
                     <td class="auto-style2">
                         <asp:TextBox ID="billNoTextBox" runat="server" CssClass="input" width="300px"></asp:TextBox>
                     </td>
                     <td class="auto-style2">
                         <asp:Button ID="searchButton" runat="server" CssClass="button" Style="margin-top:15px; margin-left:10px;" Text="Search" OnClick="searchButton_Click" />
                     </td>
                </tr>
            </table>

                        <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999" Width="80%" style="margin-left: 10%;" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                            <Columns>
                                <asp:TemplateField HeaderText="SL">
                                       <ItemTemplate>
                                         <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                     </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>

            <table>
                <tr>
                    <td>
                        <asp:Label ID="billDateLabel" runat="server" CssClass="label" Text="Bill Date"></asp:Label>
                    </td>
                      <td class="auto-style3">
                        <asp:Label ID="dateLabel" runat="server" CssClass="label" Style="margin-left:60px;float:right;" Text="< Bill Amount >" Width="200px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="totalFeeLabel" runat="server" CssClass="label" Text="Total Fee"></asp:Label>
                    </td>
                      <td class="auto-style3">
                        <asp:Label ID="totalAmountLabel" runat="server" CssClass="label" Style="margin-left:60px;float:right;" Text="< Amount >" Width="200px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="paidAmountLabel" runat="server" CssClass="label" Text="Paid Amount"></asp:Label>
                    </td>
                      <td class="auto-style3">
                        <asp:Label ID="pAmountLabel" runat="server" CssClass="label" Style="margin-left:60px;float:right;" Text="< Amount >" Width="200px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="dueAmountLabel" runat="server" CssClass="label" Text="Due Amount"></asp:Label>
                    </td>
                      <td class="auto-style3">
                        <asp:Label ID="dAmountLabel" runat="server" CssClass="label" Style="margin-left:60px;float:right;" Text="< Amount >" Width="200px"></asp:Label>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>
                         <asp:Label ID="amountLabel" runat="server" CssClass="label" Text="Amount"></asp:Label>
                    </td>
                    <td>
                            <asp:TextBox ID="amountTextBox" runat="server" CssClass="input" Style="margin-left:45px; margin-top:30px;" width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="payButton" runat="server" CssClass="button" Style="margin-top:15px; margin-left:110px;" Text="Pay" OnClick="payButton_Click" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
    </form>
</body>
</html>
