﻿using System;

namespace DiagnosticManagement.DataAccess.Models
{
    [Serializable]
    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Fee { get; set; }
        public TestType Type;

        public Test(string name, TestType type, double fee)
        {
            Name = name;
            Type = type;
            Fee = fee;
        }

        public Test()
        {
            
        }
    }
}