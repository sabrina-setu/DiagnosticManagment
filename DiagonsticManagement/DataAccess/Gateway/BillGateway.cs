﻿using System;
using System.Data;
using System.Data.SqlClient;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.DataAccess.Gateway
{
    public class BillGateway
    {
        PatientGateway _patientGateway=new PatientGateway();
        public bool SetBill(Bill bill)
        {
            string query =
                "INSERT INTO Bill(BillNo, BillDate, PatientID, TotalAmmount, DueAmmount, PaidAmmount) VALUES('"+bill.BillNo+"'" +
                ",'"+bill.BillDate.ToString("yyyy-MM-dd")+"'" +
                ","+bill.Patient.Id+"" +
                ","+bill.TotalAmmount+","+bill.DueAmmount+","+bill.PaidAmmount+");";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand setCommand=new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            int rowCount = setCommand.ExecuteNonQuery();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public Bill GetBillByNo(string billNo)
        {
            Patient patient=new Patient();
            int patientid=0;
            Bill bill = null;
            string query = "SELECT * FROM Bill WHERE BillNo='" + billNo + "';";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand=new SqlCommand(query,ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    patientid = Convert.ToInt32(getReader["PatientID"]);
                    bill=new Bill(patient);
                    bill.Id = Convert.ToInt32(getReader["ID"]);
                    bill.BillDate = (DateTime) getReader["BillDate"];
                    bill.TotalAmmount = Convert.ToDouble(getReader["TotalAmmount"]);
                    bill.DueAmmount = Convert.ToDouble(getReader["DueAmmount"]);
                    bill.PaidAmmount = Convert.ToDouble(getReader["PaidAmmount"]);
                }
            }
            getReader.Close();
            patient = _patientGateway.GetPatientById(patientid);
            patient.Tests = _patientGateway.GetAllTestofPatient(patient);
            bill.Patient = patient;
            if(ConnectionGateway.DiagosoticConnection.State==ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return bill;
        }

        public bool PayBill(Bill bill, double ammount)
        {
            string query = "UPDATE Bill SET PaidAmmount=PaidAmmount+" + ammount + ";";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand payCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            int rowCount = payCommand.ExecuteNonQuery();
            ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public DataTable UnpaidBillReport(DateTime from, DateTime to)
        {
            string query =
                "SELECT BillNo, Contact, PatientName, DueAmmount FROM UnpaidBillReport" +
                " WHERE BillDate>='" + from.ToString("yyyy-MM-dd") + "' AND BillDate<='" + to.ToString("yyyy-MM-dd") + "';";
            DataTable reportTable = new DataTable();
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)                
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand report = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader reader = report.ExecuteReader();
            reportTable.Load(reader);
            reader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return reportTable;
        }
    }
}