﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.DataAccess.Gateway
{
    public class PatientGateway
    {
        TestTypeGateway _testTypeGateway=new TestTypeGateway();

        public bool AddPatient(Patient patient)
        {
            string patientquery = "INSERT INTO Patient(PatientName	,DOB ,Contact) VALUES('"+patient.Name+"', '"+patient.Dob+"', '"+patient.Contact+"');";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand addCommand=new SqlCommand(patientquery, ConnectionGateway.DiagosoticConnection);
            int rowCount = addCommand.ExecuteNonQuery();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount >0) return true;
            return false;
        }

        public bool PatientTest(Patient patient, List<Test> tests)
        {

            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            int rowCount=0;
            foreach (Test variable in tests)
            {
                string testquery = "INSERT INTO PatientTest(PatientID,TestID,TestDate) " +
                                   "VALUES("+patient.Id+","+variable.Id+",'"+DateTime.Now.ToString("yyyy-MM-dd")+"');";
                SqlCommand patientTestCommand=new SqlCommand(testquery,ConnectionGateway.DiagosoticConnection);
                rowCount += patientTestCommand.ExecuteNonQuery();
            }
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public int GetLastPatientId()
        {
            int id=0;
            string query = "SELECT * from Patient WHERE ID = (SELECT MAX(ID) FROM Patient);";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand=new SqlCommand(query,ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    id = Convert.ToInt32(getReader["ID"]);
                }
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return id;
        }

        public Patient GetPatientById(int id)
        {

            Patient patient = new Patient();
            string query = "SELECT * from Patient WHERE ID = "+id+";";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand getCommand = new SqlCommand(query, ConnectionGateway.DiagosoticConnection);
            SqlDataReader getPatient = getCommand.ExecuteReader();
            if (getPatient.HasRows)
            {
                while (getPatient.Read())
                {
                    patient.Id = Convert.ToInt32(getPatient["ID"]);
                    patient.Name = getPatient["PatientName"].ToString();
                    patient.Dob = (DateTime)getPatient["DOB"];
                    patient.Contact = getPatient["Contact"].ToString();
                }
            }
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return patient;
        }

        public List<Test> GetAllTestofPatient(Patient patient)
        {
            List<Test> tests=new List<Test>();
            string query = "SELECT * FROM GetAllTestOfPatient WHERE PatientID=" + patient.Id + ";";
            if (ConnectionGateway.DiagosoticConnection.State != ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Open();
            SqlCommand gettestCommand = new SqlCommand(query,ConnectionGateway.DiagosoticConnection);
            SqlDataReader getReader = gettestCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    TestType testType=new TestType();
                    testType.TypeName = getReader["TypeName"].ToString();
                    Test test=new Test(getReader["TestName"].ToString(),
                        testType,
                        Convert.ToDouble(getReader["Fee"]));
                    test.Id = Convert.ToInt32(getReader["ID"]);

                    tests.Add(test);
                }
            }
            getReader.Close();
            if (ConnectionGateway.DiagosoticConnection.State == ConnectionState.Open)
                ConnectionGateway.DiagosoticConnection.Close();
            return tests;
        }
    }
}