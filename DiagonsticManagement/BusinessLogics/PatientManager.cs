﻿using DiagnosticManagement.DataAccess.Gateway;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.BusinessLogics
{
    public class PatientManager
    {
        PatientGateway _patientGateway=new PatientGateway();

        public string AddPatient(Patient patient)
        {
            if (_patientGateway.AddPatient(patient)) return "Patient Added.";
            return "Patient didnt added.";
        }

        public int GetLastPatientId()
        {
            return _patientGateway.GetLastPatientId();
        }
    }
}