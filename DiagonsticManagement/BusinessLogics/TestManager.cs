﻿using System;
using System.Collections.Generic;
using System.Data;
using DiagnosticManagement.DataAccess.Gateway;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.BusinessLogics
{
    public class TestManager
    {
        TestGateway _testGateway=new TestGateway();

        public string AddTest(Test test)
        {
            if (_testGateway.IsDuplicate(test)) return "Test already added.";
            if (_testGateway.AddTest(test)) return "Test Added.";
            return "Test didnt added.";
        }

        public List<Test> GetAllTests()
        {
            return _testGateway.GetAllTests();
        }

        public DataTable GetTestWiseReport(DateTime from, DateTime to)
        {
            return _testGateway.GetTestWiseReport(from, to);
        }

        public Test GetTestById(int id)
        {
            return _testGateway.GetTestById(id);
        }
    }
}