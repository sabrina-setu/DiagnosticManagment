﻿using System;
using System.Collections.Generic;
using System.Data;
using DiagnosticManagement.DataAccess.Gateway;
using DiagnosticManagement.DataAccess.Models;

namespace DiagnosticManagement.BusinessLogics
{
    public class TestTypeManager
    {
        private TestTypeGateway _testTypeGateway = new TestTypeGateway();

        public string AddTestType(TestType testType)
        {
            if (_testTypeGateway.IsDuplicate(testType)) return "Type Already Added.";
            if (_testTypeGateway.AddTestType(testType)) return "Test Type Added.";
            return "Test Type didn't added.";
        }

        public List<TestType> GetAllTestTypes()
        {
            return _testTypeGateway.GetAllTestTypes();
        }

        public DataTable GetTypeWiseReport(DateTime from, DateTime to)
        {
            return _testTypeGateway.GetTypeWiseReport(from, to);
        }
    }
}