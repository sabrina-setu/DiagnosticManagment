USE [master]
GO
/****** Object:  Database [CareBITMDiagonstic]    Script Date: 4/30/2017 1:18:28 AM ******/
CREATE DATABASE [CareBITMDiagonstic]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CareBITMDiagonstic', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CareBITMDiagonstic.ndf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CareBITMDiagonstic_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CareBITMDiagonstic_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CareBITMDiagonstic] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CareBITMDiagonstic].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CareBITMDiagonstic] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET ARITHABORT OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CareBITMDiagonstic] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CareBITMDiagonstic] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CareBITMDiagonstic] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CareBITMDiagonstic] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET RECOVERY FULL 
GO
ALTER DATABASE [CareBITMDiagonstic] SET  MULTI_USER 
GO
ALTER DATABASE [CareBITMDiagonstic] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CareBITMDiagonstic] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CareBITMDiagonstic] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CareBITMDiagonstic] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CareBITMDiagonstic] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CareBITMDiagonstic', N'ON'
GO
ALTER DATABASE [CareBITMDiagonstic] SET QUERY_STORE = OFF
GO
USE [CareBITMDiagonstic]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CareBITMDiagonstic]
GO
/****** Object:  UserDefinedFunction [dbo].[GetTestWiseReport]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetTestWiseReport](@FromDate date, @ToDate date)
RETURNS @ReportTable TABLE(
	SL int identity(1,1),
	TestName varchar(255),
	TotalTest int,
	TotalAmmount float
	)
AS	
BEGIN

	insert into @ReportTable(TestName, TotalTest, TotalAmmount)
	SELECT
	dbo.Test.TestName,
	COUNT(dbo.PatientTest.TestID),
	COUNT(dbo.PatientTest.TestID)*Test.Fee
	FROM
	dbo.Test
	INNER JOIN dbo.PatientTest ON dbo.PatientTest.TestID = dbo.Test.ID
	WHERE TestDate>=@FromDate AND TestDate<=@ToDate
	GROUP BY TestName,Fee;

	insert into @ReportTable(TestName, TotalTest, TotalAmmount)
	SELECT TestName, 0, 0 FROM Test
	WHERE Test.TestName not in (SELECT TestName from @ReportTable);

RETURN;
END;
GO
/****** Object:  UserDefinedFunction [dbo].[GetTypeWiseReport]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetTypeWiseReport](@FromDate date, @ToDate date)
RETURNS @ReportTable TABLE(
	SL int identity(1,1),
	TypeName varchar(255),
	TotalTest int,
	TotalAmmount float
	)
AS	
BEGIN

	insert into @ReportTable(TypeName, TotalTest, TotalAmmount)
	SELECT
	dbo.TestType.TypeName,
	COUNT(dbo.PatientTest.TestID),
	COUNT(dbo.PatientTest.TestID)*dbo.Test.Fee

	FROM
	dbo.PatientTest
	INNER JOIN dbo.Test ON dbo.PatientTest.TestID = dbo.Test.ID
	INNER JOIN dbo.TestType ON dbo.Test.TestType = dbo.TestType.ID
	WHERE dbo.PatientTest.TestDate>=@FromDate AND dbo.PatientTest.TestDate<=@ToDate
	GROUP BY TestType.TypeName,Test.Fee;

	insert into @ReportTable(TypeName, TotalTest, TotalAmmount)
	SELECT TypeName, 0, 0 FROM TestType
	WHERE TestType.TypeName not in (SELECT TypeName from @ReportTable);

RETURN;
END;
GO
/****** Object:  Table [dbo].[TestType]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Test]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TestName] [varchar](255) NOT NULL,
	[Fee] [float] NULL,
	[TestType] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[TestWithTestType]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TestWithTestType] AS 
SELECT
dbo.Test.ID,
dbo.Test.TestName,
dbo.Test.Fee,
dbo.TestType.ID as TypeId,
dbo.TestType.TypeName

FROM
dbo.Test
INNER JOIN dbo.TestType ON dbo.Test.TestType = dbo.TestType.ID
GO
/****** Object:  Table [dbo].[PatientTest]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientTest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[TestID] [int] NOT NULL,
	[TestDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[GetAllTestOfPatient]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GetAllTestOfPatient]
AS
SELECT        dbo.Test.ID, dbo.Test.TestName, dbo.Test.Fee, dbo.PatientTest.PatientID, dbo.TestType.TypeName
FROM            dbo.PatientTest INNER JOIN
                         dbo.Test ON dbo.PatientTest.TestID = dbo.Test.ID INNER JOIN
                         dbo.TestType ON dbo.Test.TestType = dbo.TestType.ID

GO
/****** Object:  Table [dbo].[Patient]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientName] [varchar](255) NOT NULL,
	[DOB] [date] NULL,
	[Contact] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bill]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BillNo] [varchar](255) NOT NULL,
	[BillDate] [date] NOT NULL,
	[PatientID] [int] NOT NULL,
	[TotalAmmount] [float] NULL,
	[DueAmmount] [float] NULL,
	[PaidAmmount] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [BillNoUK] UNIQUE NONCLUSTERED 
(
	[BillNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[UnpaidBillReport]    Script Date: 4/30/2017 1:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UnpaidBillReport] AS 
SELECT
dbo.Bill.BillNo,
dbo.Patient.Contact,
dbo.Patient.PatientName,
dbo.Bill.DueAmmount,
dbo.Bill.BillDate

FROM
dbo.Patient
INNER JOIN dbo.Bill ON dbo.Bill.PatientID = dbo.Patient.ID
WHERE Bill.DueAmmount>0

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Test]    Script Date: 4/30/2017 1:18:28 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Test] ON [dbo].[Test]
(
	[TestName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TestType]    Script Date: 4/30/2017 1:18:28 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TestType] ON [dbo].[TestType]
(
	[TypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [BillPatientFK] FOREIGN KEY([PatientID])
REFERENCES [dbo].[Patient] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [BillPatientFK]
GO
ALTER TABLE [dbo].[PatientTest]  WITH CHECK ADD  CONSTRAINT [PatientFK] FOREIGN KEY([PatientID])
REFERENCES [dbo].[Patient] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PatientTest] CHECK CONSTRAINT [PatientFK]
GO
ALTER TABLE [dbo].[PatientTest]  WITH CHECK ADD  CONSTRAINT [TestFK] FOREIGN KEY([TestID])
REFERENCES [dbo].[Test] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PatientTest] CHECK CONSTRAINT [TestFK]
GO
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [TestTypeFK] FOREIGN KEY([TestType])
REFERENCES [dbo].[TestType] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [TestTypeFK]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PatientTest"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Test"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TestType"
            Begin Extent = 
               Top = 6
               Left = 457
               Bottom = 102
               Right = 627
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetAllTestOfPatient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GetAllTestOfPatient'
GO
USE [master]
GO
ALTER DATABASE [CareBITMDiagonstic] SET  READ_WRITE 
GO
