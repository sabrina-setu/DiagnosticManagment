ALTER FUNCTION [dbo].[GetTestWiseReport](@FromDate date, @ToDate date)
RETURNS @ReportTable TABLE(
	SL int identity(1,1),
	TestName varchar(255),
	TotalTest int,
	TotalAmmount float
	)
AS	
BEGIN

	insert into @ReportTable(TestName, TotalTest, TotalAmmount)
	SELECT
	dbo.Test.TestName,
	COUNT(dbo.PatientTest.TestID),
	COUNT(dbo.PatientTest.TestID)*Test.Fee
	FROM
	dbo.Test
	INNER JOIN dbo.PatientTest ON dbo.PatientTest.TestID = dbo.Test.ID
	WHERE TestDate>=@FromDate AND TestDate<=@ToDate
	GROUP BY TestName,Fee;

	insert into @ReportTable(TestName, TotalTest, TotalAmmount)
	SELECT TestName, 0, 0 FROM Test
	WHERE Test.ID not in (SELECT TestID from PatientTest);

RETURN;
END;